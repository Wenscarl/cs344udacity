#include "utils.h"
#include <thrust/host_vector.h>
const int MAX_THREADS_PER_BLOCK = 512;
// const unsigned int TASK0 = 0;
// const unsigned int TASK1 = 1;
/* Red Eye Removal
   ===============
   
   For this assignment we are implementing red eye removal.  This is
   accomplished by first creating a score for every pixel that tells us how
   likely it is to be a red eye pixel.  We have already done this for you - you
   are receiving the scores and need to sort them in ascending order so that we
   know which pixels to alter to remove the red eye.

   Note: ascending order == smallest to largest

   Each score is associated with a position, when you sort the scores, you must
   also move the positions accordingly.

   Implementing Parallel Radix Sort with CUDA
   ==========================================

   The basic idea is to construct a histogram on each pass of how many of each
   "digit" there are.   Then we scan this histogram so that we know where to put
   the output of each digit.  For example, the first 1 must come after all the
   0s so we have to know how many 0s there are to be able to start moving 1s
   into the correct position.

   1) Histogram of the number of occurrences of each digit
   2) Exclusive Prefix Sum of Histogram
   3) Determine relative offset of each digit
        For example [0 0 1 1 0 0 1]
                ->  [0 1 0 1 2 3 2]
   4) Combine the results of steps 2 & 3 to determine the final
      output location for each element and move it there

   LSB Radix sort is an out-of-place sort and you will need to ping-pong values
   between the input and output buffers we have provided.  Make sure the final
   sorted results end up in the output buffer!  Hint: You may need to do a copy
   at the end.

 */

__global__ 
void blelloch_scan( unsigned int* d_in, size_t input_size, unsigned int* d_bias) 
/*input_size is the size of d_in, and must be > 2 * max_thread_per_block */ 
   {
      extern __shared__ unsigned int data[];
      int tid = threadIdx.x;
      int offset = 1;
      int abs_start = 2*blockDim.x*blockIdx.x;
      
      data[2 * tid] =(abs_start+2*tid)<input_size? d_in[abs_start+2 * tid]:0;
      data[2 * tid+1] = (abs_start + 2 * tid+1)<input_size ? d_in[abs_start+2 * tid+1]:0;
   
      for (int d = (2 * blockDim.x) >>1; d>0; d>>=1) {
         __syncthreads();
         
         if (tid < d) {
            int ai = offset*(2 * tid + 1) - 1;
            int bi = offset*(2 * tid + 2) - 1;
            
            data[bi] += data[ai];
         }
         offset <<= 1;
      }
      if (tid == 0)data[2*blockDim.x - 1] = 0;
   
      for (int d = 1; d < 2 * blockDim.x; d<<=1) {
         offset >>= 1;
         __syncthreads();
         if (tid < d) {
            int ai = offset*(2 * tid + 1) - 1;
            int bi = offset*(2 * tid + 2) - 1;
            unsigned int t = data[ai];
            data[ai] = data[bi];
            data[bi] += t;
         }
      }
   
      __syncthreads();
      
      unsigned shift = d_bias[blockIdx.x];
      if (abs_start + 2 * tid < input_size) {
         d_in[abs_start + 2 * tid] = data[2 * tid] + shift;
      }
      if (abs_start + 2 * tid+1 < input_size) {
         d_in[abs_start + 2 * tid+1] = data[2 * tid+1] + shift;
      }
   
   }


__global__
void Count1sBlockWise(unsigned int * d_in, unsigned int *d01counts, const int size)
{
    __shared__ unsigned cnt1s ;
   if (threadIdx.x ==0 ) cnt1s = 0;
   unsigned id = 2*threadIdx.x + 2*blockDim.x * blockIdx.x;
   if(id <size){
   if (d_in[id] == 1 ) 
     atomicAdd( &cnt1s,1);
   }
   
   id++;
   if(id <size){
      if (d_in[id] == 1  ) 
      atomicAdd( &cnt1s,1);
      }
      __syncthreads();
   if (0==threadIdx.x)  
   {
      d01counts[blockIdx.x] = cnt1s;
   }
}

void blelloch_scan_across_block(unsigned int* d_input,  const size_t numElems)
/*
Note using <2*max_thread_per_block numElems per block 
1. do histogram scan , see how many 1s in each block
2. do block scan
*/
{
   unsigned int totalBlocks = (numElems/2 + MAX_THREADS_PER_BLOCK - 1) / MAX_THREADS_PER_BLOCK ;
   
   unsigned int * h_01counts,*h_01bias ; 
   h_01counts = (unsigned int*)malloc(totalBlocks*sizeof(unsigned));
   h_01bias = (unsigned int*)malloc(totalBlocks*sizeof(unsigned));
   memset(h_01bias,0,sizeof(unsigned)*totalBlocks);

   unsigned int* d_01counts;
   unsigned int *d_01bias;
   checkCudaErrors(cudaMalloc((void**)&d_01counts,totalBlocks*sizeof(unsigned )));
   checkCudaErrors(cudaMalloc((void**)&d_01bias,totalBlocks*sizeof(unsigned )));

   Count1sBlockWise<<<totalBlocks,MAX_THREADS_PER_BLOCK>>>( d_input,d_01counts,numElems);
   checkCudaErrors(cudaMemcpy(h_01counts,d_01counts,sizeof(unsigned)*totalBlocks,cudaMemcpyDeviceToHost) );
   cudaDeviceSynchronize();
   for(unsigned int i = 1 ; i < totalBlocks;i++)
   {
      h_01bias[i] = h_01bias[i-1] + h_01counts[i-1];
   }

   checkCudaErrors(cudaMemcpy(d_01bias,h_01bias,sizeof(unsigned)*totalBlocks,cudaMemcpyHostToDevice) );

   blelloch_scan<<<totalBlocks, MAX_THREADS_PER_BLOCK, MAX_THREADS_PER_BLOCK*2*sizeof(unsigned)>>>
   (d_input,numElems,d_01bias);
   checkCudaErrors(cudaGetLastError());
//       for (int i = 0 ; i <totalBlocks;i++ )
//    printf("%d : %d, %d\n",i,h_01bias[i], h_01counts[i]);
//   printf("there are %d blocks\n",totalBlocks);
  cudaDeviceSynchronize();


 checkCudaErrors(cudaFree(d_01bias));
  checkCudaErrors(cudaFree(d_01counts));
   free(h_01counts);
   free(h_01bias);
}
__global__ void extract_bit(unsigned int* d_inVal, unsigned int* d_splitVals,
   const size_t numElems,  unsigned int ibit)
{
   int id = blockIdx.x*blockDim.x + threadIdx.x;   
   if (id >= numElems) return;
// Split based on whether inputVals digit is 1 or 0:
d_splitVals[id] = !( (d_inVal[id] >> ibit) & 1);
}

__global__ void compute_outputPos(const unsigned int* d_inputVals,
   unsigned int* d_outputVals,
   unsigned int* d_outputPos, unsigned int* d_tVals,
   const unsigned int* d_splitVals,
   const unsigned int* d_cdf, const unsigned int totalFalses,
   const unsigned int numElems)
{

int thid = threadIdx.x;
int global_id = blockIdx.x*blockDim.x + thid;
if (global_id >= numElems) return;

d_tVals[global_id] = global_id - d_cdf[global_id] + totalFalses;

unsigned int scatter = (d_splitVals[global_id])? d_cdf[global_id] :d_tVals[global_id];
d_outputPos[global_id] = scatter;

}

__global__ void ScatterAdd(unsigned int* d_outputVals, const unsigned int* d_inputVals,
   unsigned int* d_outputPos,
   unsigned int* d_inputPos,
   unsigned int* d_scatterAddr,
   const unsigned int numElems)
{

int global_id = blockIdx.x*blockDim.x + threadIdx.x;
if(global_id >= numElems) return;

d_outputVals[d_outputPos[global_id]]  = d_inputVals[global_id];
d_scatterAddr[d_outputPos[global_id]] = d_inputPos[global_id];

}

void compute_scatter_addresses(const unsigned int* d_inputVals,
   unsigned int* d_outputVals,
   unsigned int* d_inputPos,
   unsigned int* d_outputPos,
   unsigned int* d_scatterAddr,
   const unsigned int* const d_splitVals,
   const unsigned int* const d_cdf,
   const unsigned totalOnes,
   const size_t numElems)
/*
Modifies d_outputVals and d_outputPos
*/
{

unsigned int* d_tVals;
checkCudaErrors(cudaMalloc((void**)&d_tVals, numElems*sizeof(unsigned)));


int nblocks  = (numElems - 1 + MAX_THREADS_PER_BLOCK ) / MAX_THREADS_PER_BLOCK;
compute_outputPos<<<nblocks, MAX_THREADS_PER_BLOCK>>>(d_inputVals, d_outputVals, d_outputPos,
               d_tVals, d_splitVals, d_cdf, totalOnes,  numElems);
 checkCudaErrors(cudaGetLastError());

 ScatterAdd<<<nblocks, MAX_THREADS_PER_BLOCK>>>(d_outputVals, d_inputVals, d_outputPos,
        d_inputPos, d_scatterAddr, numElems);
 checkCudaErrors(cudaGetLastError());

checkCudaErrors(cudaFree(d_tVals));

}



void your_sort(unsigned int* const d_inputVals,
               unsigned int* const d_inputPos,
               unsigned int* const d_outputVals,
               unsigned int* const d_outputPos,
               const size_t numElems)
{ 
  //TODO
  //PUT YOUR SORT HERE
  unsigned int* d_splitVals,*d_cdf,*d_scat_add; 
  checkCudaErrors(cudaMalloc((void**)&d_splitVals, numElems*sizeof(unsigned)));
  checkCudaErrors(cudaMalloc((void**)&d_cdf, numElems*sizeof(unsigned)));
  checkCudaErrors(cudaMalloc((void**)&d_scat_add, numElems*sizeof(unsigned)));
  checkCudaErrors(cudaMemcpy(d_scat_add, d_inputPos, numElems*sizeof(unsigned),    cudaMemcpyDeviceToDevice));


  unsigned int* d_ptr_inVals = d_inputVals;
  unsigned int* d_ptr_inPos = d_inputPos;
  unsigned int* d_ptr_outVals = d_outputVals;
  unsigned int* d_ptr_outPos = d_outputPos;

  unsigned int  h_cdflast ;
  unsigned int h_splitlast;

  int numBlocks = (numElems + MAX_THREADS_PER_BLOCK - 1)/MAX_THREADS_PER_BLOCK; 
  for (unsigned int digs = 0 ; digs < 32; digs++)
  {
      checkCudaErrors(cudaMemset(d_splitVals, 0, numElems*sizeof(unsigned)));
      checkCudaErrors(cudaMemset(d_cdf,0,numElems*sizeof(unsigned)));

      extract_bit<<<numBlocks,MAX_THREADS_PER_BLOCK>>>(d_ptr_inVals,d_splitVals,numElems,digs);
      checkCudaErrors(cudaGetLastError());
      
      checkCudaErrors(cudaMemcpy(d_cdf,d_splitVals,sizeof(unsigned int)*numElems,cudaMemcpyDeviceToDevice));
       blelloch_scan_across_block(d_cdf, numElems);

       //compute total ones
       
       checkCudaErrors(cudaMemcpy(&h_splitlast,d_splitVals + numElems-1 ,sizeof(unsigned ),cudaMemcpyDeviceToHost));
       checkCudaErrors(cudaMemcpy(&h_cdflast,d_cdf + numElems-1,sizeof(unsigned ),cudaMemcpyDeviceToHost));

       compute_scatter_addresses(d_ptr_inVals, d_ptr_outVals, d_ptr_inPos, d_ptr_outPos, d_scat_add,
         d_splitVals, d_cdf, h_cdflast +h_splitlast , numElems);

      std::swap(d_ptr_inVals, d_ptr_outVals);
      std::swap(d_ptr_inPos, d_scat_add);
      
      }
      checkCudaErrors(cudaMemcpy(d_outputPos, d_ptr_inPos, numElems*sizeof(unsigned), cudaMemcpyDeviceToDevice));

      checkCudaErrors(cudaFree(d_splitVals));
      checkCudaErrors(cudaFree(d_scat_add));
      checkCudaErrors(cudaFree(d_cdf));

}

