/* Udacity Homework 3
   HDR Tone-mapping

  Background HDR
  ==============

  A High Dynamic Range (HDR) image contains a wider variation of intensity
  and color than is allowed by the RGB format with 1 byte per channel that we
  have used in the previous assignment.  

  To store this extra information we use single precision floating point for
  each channel.  This allows for an extremely wide range of intensity values.

  In the image for this assignment, the inside of church with light coming in
  through stained glass windows, the raw input floating point values for the
  channels range from 0 to 275.  But the mean is .41 and 98% of the values are
  less than 3!  This means that certain areas (the windows) are extremely bright
  compared to everywhere else.  If we linearly map this [0-275] range into the
  [0-255] range that we have been using then most values will be mapped to zero!
  The only thing we will be able to see are the very brightest areas - the
  windows - everything else will appear pitch black.

  The problem is that although we have cameras capable of recording the wide
  range of intensity that exists in the real world our monitors are not capable
  of displaying them.  Our eyes are also quite capable of observing a much wider
  range of intensities than our image formats / monitors are capable of
  displaying.

  Tone-mapping is a process that transforms the intensities in the image so that
  the brightest values aren't nearly so far away from the mean.  That way when
  we transform the values into [0-255] we can actually see the entire image.
  There are many ways to perform this process and it is as much an art as a
  science - there is no single "right" answer.  In this homework we will
  implement one possible technique.

  Background Chrominance-Luminance
  ================================

  The RGB space that we have been using to represent images can be thought of as
  one possible set of axes spanning a three dimensional space of color.  We
  sometimes choose other axes to represent this space because they make certain
  operations more convenient.

  Another possible way of representing a color image is to separate the color
  information (chromaticity) from the brightness information.  There are
  multiple different methods for doing this - a common one during the analog
  television days was known as Chrominance-Luminance or YUV.

  We choose to represent the image in this way so that we can remap only the
  intensity channel and then recombine the new intensity values with the color
  information to form the final image.

  Old TV signals used to be transmitted in this way so that black & white
  televisions could display the luminance channel while color televisions would
  display all three of the channels.
  

  Tone-mapping
  ============

  In this assignment we are going to transform the luminance channel (actually
  the log of the luminance, but this is unimportant for the parts of the
  algorithm that you will be implementing) by compressing its range to [0, 1].
  To do this we need the cumulative distribution of the luminance values.

  Example
  -------

  input : [2 4 3 3 1 7 4 5 7 0 9 4 3 2]
  min / max / range: 0 / 9 / 9

  histo with 3 bins: [4 7 3]

  cdf : [4 11 14]


  Your task is to calculate this cumulative distribution by following these
  steps.

*/

#include "utils.h"
#include "stdio.h"
__global__
void scan_kernel(unsigned int * d_bins,unsigned int* d_cdf, unsigned int *blockSum ,size_t numItems)
{
   extern __shared__ unsigned int tmp_data[];
 unsigned mid = threadIdx.x + blockDim.x* blockIdx.x;
 if(mid >= numItems) return;
  unsigned myval = d_bins[mid]; 
tmp_data[threadIdx.x] = myval;
 
    for(int s = 1 ; s <= numItems; s<<1){
    unsigned int spot = mid - s;
    unsigned int val = 0.f;
     if(spot >=0)
        val = tmp_data[spot];
     __syncthreads();
     if(spot>=0)
     tmp_data[mid] += val;
     __syncthreads();
     }
     if (0==threadIdx.x) blockSum[blockIdx.x] = tmp_data[blockDim.x-1]; 
     d_cdf[mid] = tmp_data[mid] - myval;//exclusive scan now
}

__global__
void global_scan(unsigned int* d_cdf, unsigned int* blockSum, const size_t size)
{
   int mid = threadIdx.x + blockDim.x * blockIdx.x;
    if(mid >= size)
        return;
    if (blockIdx.x>0 )
    {
       d_cdf[mid]+=blockSum[blockIdx.x-1];
    }
}

__global__
void histogram_kernel(unsigned int* d_bins, const float* d_in, const int bin_count, const float lum_min, const float lum_max, const int size) {  
    int mid = threadIdx.x + blockDim.x * blockIdx.x;
    if(mid >= size)
        return;
    float lum_range = lum_max - lum_min;
    unsigned bin = ((d_in[mid]-lum_min) / lum_range) * bin_count;
    
    atomicAdd(&d_bins[bin], 1);
}
__global__ void reduce_min(float const * d_in, float *d_out,const size_t numItem)
{
 extern __shared__ float sdata[];
int glbid = blockIdx.x * blockDim.x + threadIdx.x;
int tid = threadIdx.x;
 sdata[tid] = 99999999999.f;
 if(glbid<numItem)
 sdata[tid] = d_in[glbid];
 __syncthreads();

for(int s = blockDim.x / 2 ; s > 0 ; s>>1 )
{
   if(tid<s)
     sdata[tid] = min(sdata[tid],sdata[tid+s]);
   __syncthreads();
}
 
if(tid==0)
d_out[blockIdx.x] = sdata[0];
  
}
__global__ void reduce_max(float const * d_in, float *d_out,const size_t numItem)
{
 extern __shared__ float sdata[];
int glbid = blockIdx.x * blockDim.x + threadIdx.x;
int tid = threadIdx.x;
 sdata[tid] = -99999999999.f;
 if(glbid<numItem)
 sdata[tid] = d_in[glbid];
 __syncthreads();

for(int s = blockDim.x / 2 ; s > 0 ; s>>1 )
{
   if(tid<s)
     sdata[tid] = max(sdata[tid],sdata[tid+s]);
   __syncthreads();
}
 
if(tid==0)
d_out[blockIdx.x] = sdata[0];
  
}
void your_histogram_and_prefixsum(const float* const d_logLuminance,
                                  unsigned int* const d_cdf,
                                  float &min_logLum,
                                  float &max_logLum,
                                  const size_t numRows,
                                  const size_t numCols,
                                  const size_t numBins)
{
  //TODO
  /*Here are the steps you need to implement
    1) find the minimum and maximum value in the input logLuminance channel
       store in min_logLum and max_logLum
    2) subtract them to find the range
    3) generate a histogram of all the values in the logLuminance channel using
       the formula: bin = (lum[i] - lumMin) / lumRange * numBins
    4) Perform an exclusive scan (prefix sum) on the histogram to get
       the cumulative distribution of luminance values (this should go in the
       incoming d_cdf pointer which already has been allocated for you)       */
    unsigned int numItems = numRows * numCols;
  dim3 blockSize(256, 1, 1);
  dim3 gridSize((numItems + blockSize.x-1) / blockSize.x , 1, 1);
  float *d_inter_min,* d_inter_max;
  checkCudaErrors(cudaMalloc(&d_inter_min,sizeof(float)*gridSize.x ));
  checkCudaErrors(cudaMalloc(&d_inter_max,sizeof(float)*gridSize.x ));
    
  printf("numIterms = %d\n", numItems);
  reduce_min<<<gridSize, blockSize, sizeof(float) * blockSize.x>>>(d_logLuminance,d_inter_min,  numItems);
  reduce_max<<<gridSize, blockSize, sizeof(float) * blockSize.x>>>(d_logLuminance,d_inter_max, numItems);
  numItems = gridSize.x;
  gridSize.x = (numItems + blockSize.x-1) / blockSize.x;

  printf("numIterms = %d\n", numItems);
  while(numItems > 1)
  {
  reduce_max<<<gridSize,blockSize,sizeof(float)*blockSize.x>>>(d_inter_max, d_inter_max,numItems);
  reduce_min<<<gridSize,blockSize,sizeof(float)*blockSize.x>>>(d_inter_min, d_inter_min,numItems);
  numItems = gridSize.x;
  gridSize.x = (numItems + blockSize.x-1) / blockSize.x;
  printf("numIterms = %d\n", numItems);
  }

  printf("hererere1\n");
  float h_min_logLum,h_max_logLum;
  checkCudaErrors(cudaMemcpy(&h_min_logLum,d_inter_min,sizeof(float),cudaMemcpyDeviceToHost));
  checkCudaErrors(cudaMemcpy(&h_max_logLum,d_inter_max,sizeof(float),cudaMemcpyDeviceToHost));
  min_logLum = h_min_logLum;
  max_logLum = h_max_logLum;

  printf("hererere1\n");
  numItems = numCols*numRows;
  gridSize.x = (numItems + blockSize.x-1) / blockSize.x;
  
  printf("1111111111111111111\n");
  unsigned int * d_histogram;
  checkCudaErrors(cudaMalloc(&d_histogram,numBins*sizeof(unsigned int) ));
  histogram_kernel<<<gridSize,blockSize>>>(d_histogram, d_logLuminance, numBins, h_min_logLum, h_max_logLum, numItems);
  printf("222222222222222222222222\n");
  unsigned int* blockSum;
  checkCudaErrors(cudaMalloc(&blockSum,sizeof(unsigned)* gridSize.x ));
  scan_kernel<<<gridSize,blockSize,sizeof(unsigned)*blockSize.x>>>(d_histogram, d_cdf, blockSum ,numBins);
  global_scan<<<gridSize,blockSize,sizeof(unsigned)*blockSize.x>>>(d_cdf, blockSum ,numBins);


  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
  cudaFree(d_histogram);
  checkCudaErrors(cudaFree(d_inter_min));
  checkCudaErrors(cudaFree(d_inter_max));
  checkCudaErrors(cudaFree(blockSum));
}
